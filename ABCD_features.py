import cv2
import math
import numpy as np


class ABCD_extractor:
    def __init__(self, mask=None, image=None):
        self.__mask = mask
        self.__image = image
        self.__hullP1 = None
        self.__hullP2 = None
        self.__convexHull = None
        self.__border = None

    def get_features(self, mask=None, image=None):
        if mask is None:
            if self.__mask is None:
                print('No mask informed!')
                return None
            mask = self.__mask

        if image is None:
            if self.__image is None:
                print('No image given!')
                return None
            img = self.__image

        features = np.array([0, 0, 0, 0, 0, 0, 0], dtype=np.float32)

        features[6] = self.get_diameter(mask)
        features[0] = self.get_assymetry(mask)
        border = self.get_border(mask)
        features[5] = self.get_color(image)
        features[1] = border[0]
        features[2] = border[1]
        features[3] = border[2]
        features[4] = border[3]

        return features

    def get_diameter(self, mask=None):
        if mask is None:
            if self.__mask is None:
                print('No mask informed!')
                return None
            mask = self.__mask

        # Decidido usar Laplacian aqui
        l_image = cv2.Laplacian(mask, cv2.CV_8U)

        # Pega o contorno
        im2, cntours, hier = cv2.findContours(l_image, cv2.RETR_EXTERNAL,
                                              cv2.CHAIN_APPROX_SIMPLE)

        self.__border = cntours[0]

        self.__convexHull = cv2.convexHull(cntours[0])

        diameter = 0.0

        for point in self.__convexHull:
            for otherPoint in self.__convexHull:
                distance = math.sqrt((point[0][0] - otherPoint[0][0])**2 +
                                     (point[0][1] - otherPoint[0][1])**2)

                if distance > diameter:
                    self.__hullP1 = point
                    self.__hullP2 = otherPoint
                    diameter = distance

        return diameter

    def get_assymetry(self, mask=None):
        if mask is None:
            if self.__mask is None:
                print('No mask informed!')
                return None
            mask = self.__mask

        if self.__hullP1 is None or self.__hullP2 is None:
            self.get_diameter(mask)

        a = self.__hullP1[0][1] - self.__hullP2[0][1]
        b = self.__hullP2[0][0] - self.__hullP1[0][0]
        c = self.__hullP1[0][0] * self.__hullP2[0][1] - \
            self.__hullP2[0][0] * self.__hullP1[0][1]

        assymetry = 0.0

        for point in self.__convexHull:
            distance = abs(a*point[0][0] + b*point[0][1] + c)/(math.sqrt(a**2
                                                                         + b**2))
            if distance > assymetry:
                assymetry = distance

        return assymetry

    def get_border(self, mask=None):
        if mask is None:
            if self.__mask is None:
                print('No mask informed!')
                return None
            mask = self.__mask

        if self.__border is None:
            self.get_diameter(mask)

        border = []

        for point in self.__border:
            border.append(point[0][1])

        minorPeaks = 0
        minorValleys = 0
        majorPeaks = 0
        majorValleys = 0

        lenBorder = len(border)

        for i in range(lenBorder):
            minor_value = border[i] - border[(i+1) % lenBorder]
            major_value = border[i] - border[(i+15) % lenBorder]

            if minor_value < 0:
                minorValleys += 1
            elif minor_value > 0:
                minorPeaks += 1

            if major_value < -10:
                majorValleys += 1
            elif major_value > 10:
                majorPeaks += 1

        return minorPeaks, minorValleys, majorPeaks, majorValleys

    def get_color(self, image=None, size_region=100):
        if image is None:
            if self.__image is None:
                print('No image given!')
                return None
            image = self.__image

        intervals = {}

        rows, cols = image.shape[:2]

        for i in range(rows):
            for j in range(cols):
                key = image[i, j]//5

                if key == 51:
                    key = 50

                if key not in intervals:
                    intervals[key] = 0

                intervals[key] += 1

        colors = 0

        # print(intervals)

        for key in intervals:
            if intervals[key] >= size_region:
                colors += 1

        return colors


if __name__ == '__main__':
    img = cv2.imread(
        '/home/pedropeter/Documentos/ToDO/ph2/PH2/Images/Masks/IMD065.bmp', 0)
    imgGray = cv2.imread(
        '/home/pedropeter/Documentos/ToDO/Pre-Processamento/Rois/IMD065.bmp', 0)

    ABCD = ABCD_extractor()

    print(ABCD.get_diameter(img))
    print(ABCD.get_assymetry(img))
    print(ABCD.get_color(imgGray))
    print(ABCD.get_border(img))
    print(ABCD.get_features(img, imgGray))

    # teste = mask.copy()
    # teste[:] = 0;

    # cv2.line(teste, (self.__hullP1[0][0], self.__hullP1[0][1]), (self.__hullP2[0][0], self.__hullP2[0][1]), (255, 0, 0), 8)

    # cv2.imshow("Teste", mask)

    # teste[370, 617] = 255
    # cv2.imshow("Teste2", teste)
    # cv2.waitKey(0)
