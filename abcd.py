import ABCD_features as abcd
import sys
import cv2
import numpy as np

# args ->  imgsPaths, maskPaths, targets


def doABCDExtraction(images):
    # if len(args) <= 3:
    #     sys.exit()

    # images = [line.rstrip('\n') for line in open(args[1], 'r')]
    # masks = [line.rstrip('\n') for line in open(args[2], 'r')]
    # targets = [line.rstrip('\n') for line in open(args[3], 'r')]

    # arch = open('features.libsvm', 'w')

    # counter = 0

    extractor = abcd.ABCD_extractor()

    features_return = None

    for i in range(len(images)):
        img = cv2.imread(images[i])

        if "PH2" in images[i]:
        #(images[i].find("PH2") >= 0):
            mask_name = "/media/deeplabmint/DADOS/Lucas/Melanoma/PH2/Original/Masks/" + \
            images[i][-10:]

        if "ISIC_2016" in images[i]:
        #(images[i].find("ISIC_2016") >= 0):
            mask_name = "/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC_2016/Masks/" + \
            images[i][-16:-4] + "_Segmentation.png"
        print(mask_name)

        mask = cv2.imread(mask_name, 0)

        imgHSV = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

        S = np.float32(imgHSV[:, :, 1])
        V = np.float32(imgHSV[:, :, 2])

        # print(imgHSV[:, :, 0].max())
        # print(imgHSV[:, :, 1].max())
        # print(imgHSV[:, :, 2].max())

        summedSV = cv2.add(S, V)

        summedSV = np.uint8(summedSV)

        features = extractor.get_features(mask, summedSV)

        if features_return is None:
            features_return = features
        else:
            features_return = np.vstack((features_return, features))

        # cv2.imshow("HSV", imgHSV)
        # cv2.imshow("H", imgHSV[:, :, 0])
        # cv2.imshow("S", imgHSV[:, :, 1])
        # cv2.imshow("V", imgHSV[:, :, 2])
        # cv2.imshow("Teste", summedSV)
        # cv2.waitKey(0)

    return features_return


if __name__ == '__main__':
    main(sys.argv)
