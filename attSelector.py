from collections import Counter
import setup
import featureExtractor as fe
import wekaHelper as wh

(X_train, y_train), (X_test, y_test), (X_valid, y_valid) = setup.config_base(database="deepFeatsLibSVM/gaMilagroso/dfAbcdPh2Isic_smoteen.libsvm", test_prop=0.0)

mapa_selection = wh.get_selected_attributes_from_file("deepFeatsLibSVM/weka/attSel/pre/greedy_ph2isic.txt", 1)
X_train = X_train[:, mapa_selection==1]

print("Shape de X_train_new: ")
print(X_train.shape)

with open("deepFeatsLibSVM/gaMilagroso/shapes.txt", "a") as arch:
    arch.write("Shape: " + str(X_train.shape) + "\n")

fe.toLibSVM(X_train, y_train, "deepFeatsLibSVM/gaMilagroso", "dfAbcdPh2IsicGreedy_smoteen.libsvm")
