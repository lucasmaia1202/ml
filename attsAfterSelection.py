from collections import Counter
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import SelectFromModel
from sklearn.svm import LinearSVC
import autosklearn.classification as asc
import numpy as np
import random
import setup
import featureExtractor as fe
import smoteenn
import autosvm
import scores
import abcd
import wekaHelper as wh



def main():

    # mapa_selection = wh.get_selected_attributes_from_file("deepFeatsLibSVM/weka/attSel/pre/greedy_ph2.txt", 1)
    mapa_selection = wh.get_selected_attributes_from_file("deepFeatsLibSVM/weka/attSel/pre/ranker_ph2.txt", 0.17)
    # mapa_selection = wh.get_selected_attributes_from_file("deepFeatsLibSVM/weka/attSel/pre/rankSearch_ph2.txt", 6)
    # mapa_selection = wh.get_selected_attributes_from_file("deepFeatsLibSVM/weka/attSel/pre/pso_ph2.txt", 6)

    resnetSize = 2048
    inceptionSize = 2048
    vgg16Size = 512
    vgg19Size = 512

    resnet = mapa_selection[0:resnetSize]
    vgg16 = mapa_selection[resnetSize:resnetSize+vgg16Size]
    vgg19 = mapa_selection[resnetSize+vgg16Size:resnetSize+vgg16Size+vgg19Size]
    inception = mapa_selection[resnetSize+vgg16Size+vgg19Size:resnetSize+vgg16Size+vgg19Size+inceptionSize]
    abcd = mapa_selection[resnetSize+vgg16Size+vgg19Size+inceptionSize:]


    print(len(resnet))
    print(len(vgg16))
    print(len(vgg19))
    print(len(inception))
    print(len(abcd))

    print(len(resnet[resnet==1]))
    print(len(vgg16[vgg16==1]))
    print(len(vgg19[vgg19==1]))
    print(len(inception[inception==1]))
    print(len(abcd[abcd==1]))

    print(abcd)



if __name__ == "__main__":
    main()
