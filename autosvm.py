
import autosklearn.classification as asc
from sklearn.svm import SVC
import json

def define_autosvm(searching_time):
    return asc.AutoSklearnClassifier(
        time_left_for_this_task=searching_time+10,
        per_run_time_limit=searching_time,
        ensemble_size=1,
        initial_configurations_via_metalearning=0,
        include_estimators=["libsvm_svc", ], exclude_estimators=None,
        include_preprocessors=["no_preprocessing", ], exclude_preprocessors=None)

def svm_from_autosklearn(params):
    print("Getting svm from params estimated...")

    if "SimpleClassificationPipeline" not in params:
        svm = SVC()
        print(svm)
        return svm

    index_ini = params.find("SimpleClassificationPipeline(") + len("SimpleClassificationPipeline(")
    index_fim = params.find("},") + 1

    config = params[index_ini:index_fim].replace("'", '"');

    data = json.loads(config)

    print(data)

    C = float(data["classifier:libsvm_svc:C"])
    kernel = str(data["classifier:libsvm_svc:kernel"])
    # degree
    gamma = float(data["classifier:libsvm_svc:gamma"])
    # probability
    if(data["classifier:libsvm_svc:shrinking"] == 'True'):
        shrinking = True
    else:
        shrinking = False
    tol = float(data["classifier:libsvm_svc:tol"])
    # class_weight
    # verbose
    max_iter = int(data["classifier:libsvm_svc:max_iter"])
    # decision_funciton_shape
    # random_state

    svm = SVC(C=C, kernel=kernel, gamma=gamma, shrinking=shrinking, tol=tol)
    if(kernel == 'sigmoid' or kernel == 'poly'):
        coef0 = data["classifier:libsvm_svc:coef0"]
        svm = SVC(C=C, kernel=kernel, gamma=gamma, shrinking=shrinking, tol=tol, coef0=coef0)

    return svm

def run(time=60, X=None, y=None, verbose=False):
    svm = define_autosvm(searching_time=time)

    if(verbose):
        print("Estimating a new SVM model... for " + str(time) + " seconds.")

    svm.fit(X, y)

    svm_model = svm.show_models()

    if(verbose):
        print(svm_model)

    sk_svm = svm_from_autosklearn(svm_model)

    sk_svm.fit(X, y)

    return sk_svm
