
import sklearn as sk

def predict_and_save(classifier, X_test, y_test, verbose=False, file=None):
    if(verbose):
        print('Classification results...')

    y_pred = classifier.predict(X_test)
    # print(classification_report_imbalanced(y_test, y_pred))
    cm = sk.metrics.confusion_matrix(y_test, y_pred)
    tn, fp, fn, tp = cm.ravel()
    pos = tp + fn
    neg = fp + tn
    acc = (tp + tn)/(pos + neg)
    prec = tp/(tp + fp)
    sens = tp/(tp + fn)
    spec = tn/(tn + fp)
    fscore = 2*tp/(2*tp + fp + fn)
    kappa = sk.metrics.cohen_kappa_score(y_test, y_pred)

    if(verbose):
        print("Acc\t\tPrec\t\tSens\t\tSpec\t\tFscore\t\tKappa\t\tTP\tFN\tFP\tTN")
        print("{:2f}\t{:2f}\t{:2f}\t{:2f}\t{:2f}\t{:2f}\t{:d}\t{:d}\t{:d}\t{:d}".format(acc,prec,sens,spec,fscore,kappa,tp,fn,fp,tn))

    if(file != None):
        with open(file, "a") as arch:
            arch.write("{:2f}\t{:2f}\t{:2f}\t{:2f}\t{:2f}\t{:2f}\t{:d}\t{:d}\t{:d}\t{:d}\n".format(acc,prec,sens,spec,fscore,kappa,tp,fn,fp,tn))
