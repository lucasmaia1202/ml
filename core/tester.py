from collections import Counter
import setup
import featureExtractor as fe
import smoteenn
import autosvm
import scores
import roier

# database = '/media/deeplabmint/DADOS/Lucas/Melanoma/PH2/Original/Rois_NM/'
# img_type = 'bmp'
# database = '/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC_2016/';
# img_type = 'jpg'

database = [
    {"url": '/media/deeplabmint/DADOS/Lucas/Melanoma/PH2/Original/Rois_NM/', "img_type": "bmp"},
    {"url": '/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC_2016/Rois/RoisSep/', "img_type": "jpg"}
]

# model = "VGG16"
# pooling = "max"

# (train_set, y_train), (test_set, y_test), (valid_set, y_valid) = setup.config_base(database=database, test_prop=0.3, valid_prop=0.0)

(X_train, y_train), (X_test, y_test), (X_valid, y_valid) = setup.config_base(database="deepFeatsLibSVM/dfAbcdPh2Isic.libsvm", test_prop=0.0, valid_prop=0.0)

# print(X_train.shape)
# print(X_test.shape)
# print(X_valid.shape)
#
# print(len(y_train[y_train==1]))
# print(len(y_train[y_train==0]))
#
# print(len(y_test[y_test==1]))
# print(len(y_test[y_test==0]))
#
# print(len(y_valid[y_valid==1]))
# print(len(y_valid[y_valid==0]))

# X_train = fe.extract(train_set, model, pooling)
# X_test = fe.extract(test_set, model, pooling)
#
# print('Class distribution of the training set: {}'.format(Counter(y_train)))
# print('Class distribution of the test set: {}'.format(Counter(y_test)))
#
# svm = autosvm.run(time=10, X=X_train, y=y_train, verbose=True)
#
# X_st, y_st = smoteenn.run(X=X_train, y=y_train, verbose=True, save_dist=True, file="dist.txt")
#
# svm_final = autosvm.run(time=10, X=X_st, y=y_st, verbose=True)
# scores.predict_and_save(svm_final, X_test, y_test, verbose=True, file="results.txt")
