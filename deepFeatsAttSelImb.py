from collections import Counter
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import SelectFromModel
from sklearn.svm import LinearSVC
import autosklearn.classification as asc
import numpy as np
import random
import setup
import featureExtractor as fe
import smoteenn
import autosvm
import scores
import abcd
import wekaHelper as wh


def doExtraction(set, models, pooling):
    X = []
    for m in models:
        feats = np.asarray(fe.extract(set, m, pooling))
        if(len(X) > 0):
            X = np.hstack((np.asarray(X), feats))
        else:
            X = feats

    return X


def main():
    database = "deepFeatsLibSVM/dfAbcdPh2.libsvm"

    time = 60*1
    rng = np.random.RandomState()

    runs = 10
    useAttSel = True

    for i in range(runs):

        print("Dividingo a base em Treino/Test")

        (X_train, y_train), (X_test, y_test), (X_valid, y_valid) = setup.config_base(
            database=database, test_prop=0.3)

        # MAKING ATTRIBUTES SELECTION
        if(useAttSel):
            mapa_selection = wh.get_selected_attributes_from_file("deepFeatsLibSVM/weka/attSel/pre/greedy_ph2.txt", 1)
            X_train = X_train[:, mapa_selection==1]
            X_test = X_test[:, mapa_selection==1]

            print("Shape de X_train_new: ")
            print(X_train.shape)
            print("Shape de X_test_new: ")
            print(X_test.shape)

            with open("deepFeatsAttSelImb/ph2/greedy_1/shapes.txt", "a") as arch:
                arch.write("Train: " + str(X_train.shape) + ", Test: " + str(X_test.shape) + "\n")

        # TRAINING RANDOM FOREST WITH IMBALANCED DATA
        print("Estimando Random Forest para dados Desbalanceados: Run #" + str(i+1))
        rf = asc.AutoSklearnClassifier(
            time_left_for_this_task=time+10,
            per_run_time_limit=time,
            ensemble_size=1,
            initial_configurations_via_metalearning=0,
            include_estimators=["random_forest", ], exclude_estimators=None,
            include_preprocessors=["no_preprocessing", ], exclude_preprocessors=None)

        #rf.fit(X_train, y_train)
        #scores.predict_and_save(rf, X_test, y_test, verbose=True, file="deepFeatsAttSelImb/ph2/greedy_1/split_50/RF.txt")

        # TRAINING SVM WITH IMBALANCED DATA
        print("Estimando SVM para SMOTEENN: Run #" + str(i+1))
        svm = autosvm.run(time=time+10, X=X_train, y=y_train, verbose=True)
        scores.predict_and_save(svm, X_test, y_test, verbose=True, file="deepFeatsAttSelImb/ph2/greedy_1/SVM.txt")

        # APPLYING SMOTEENN
        X_st, y_st = smoteenn.run(X=X_train, y=y_train, smote_kind="svm", smote_svm_estimator=svm, verbose=True, save_dist=True, file="deepFeatsAttSelImb/ph2/greedy_1/dist.txt")

        # TRAINING RANDOM FOREST WITH BALANCED DATA
        print("Estimando Random Forest para dados Balanceados: Run #" + str(i+1))
        rfb = asc.AutoSklearnClassifier(
            time_left_for_this_task=time+10,
            per_run_time_limit=time,
            ensemble_size=1,
            initial_configurations_via_metalearning=0,
            include_estimators=["random_forest", ], exclude_estimators=None,
            include_preprocessors=["no_preprocessing", ], exclude_preprocessors=None)

        #rfb.fit(X_st, y_st)
        #scores.predict_and_save(rfb, X_test, y_test, verbose=True, file="deepFeatsAttSelImb/ph2/greedy_1/split_50/RF_Balanced.txt")

        # TRAINING SVM WITH BALANCED DATA
        svm_final = autosvm.run(time=time, X=X_st, y=y_st, verbose=True)
        scores.predict_and_save(svm_final, X_test, y_test, verbose=True, file="deepFeatsAttSelImb/ph2/greedy_1/SVM_Balanced.txt")


    print("\n\nCABOU!!\n\n")


if __name__ == "__main__":
    main()
