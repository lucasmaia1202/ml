import numpy as np
import setup
import featureExtractor as fe
import abcd


def doExtraction(set, models, pooling):
    X = []
    for m in models:
        feats = np.asarray(fe.extract(set, m, pooling, image_size=(331, 331)))
        if(len(X) > 0):
            X = np.hstack((np.asarray(X), feats))
        else:
            X = feats

    return X


def main():
    database = [
        {"url": '/media/deeplabmint/DADOS/Lucas/Melanoma/PH2/Original/Rois_NM/', "img_type": "bmp"},
        # {"url": '/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC_2016/Rois/RoisSep/', "img_type": "jpg"}
    ]

    models = ["NASNetLarge", "DenseNet121", "DenseNet169", "DenseNet201"]
    pooling = "max"

    (train_set, y_train), (test_set, y_test), (valid_set, y_valid) = setup.config_base(
            database=database, test_prop=0.0)

    print("Extraindo caracteristica com as arquiteturas")

    X_cnn_train = doExtraction(train_set, models, pooling)

    print("Extraindo caracteristicas ABCD")

    X_abcd_train = abcd.doABCDExtraction(train_set)

    X_train = np.hstack((X_cnn_train, X_abcd_train))

    print("Exporting to LibSVM...")

    fe.toLibSVM(X_train, y_train, "deepFeatsLibSVM/newModels", "NASNetLarge.libsvm")

    print("\n\nCABOU!!\n\n")


if __name__ == "__main__":
    main()
