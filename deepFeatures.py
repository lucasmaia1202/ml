from collections import Counter
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import SelectFromModel
from sklearn.svm import LinearSVC
import autosklearn.classification as asc
import numpy as np
import random
import setup
import featureExtractor as fe
import smoteenn
import autosvm
import scores

database = '/media/deeplabmint/DADOS/Lucas/Melanoma/PH2/Original/Rois_NM/'
img_type = 'bmp'

models = ["ResNet50", "VGG16", "VGG19", "InceptionV3"]
pooling = "max"
time = 60*45
rng = np.random.RandomState()

(train_set, y_train), (test_set, y_test), (valid_set, y_valid) = setup.config_base(database=database, test_prop=0.0, img_type=img_type)

X = []
for m in models:
    feats = np.asarray(fe.extract(train_set, m, pooling))
    if(len(X) > 0):
        X = np.hstack((np.asarray(X), feats))
    else:
        X = feats


print("Shape de X: ")
print(X.shape)
y = np.asarray(y_train)

lsvc = LinearSVC(C=0.01, penalty="l1", dual=False).fit(X, y)
model = SelectFromModel(lsvc, prefit=True)
X = model.transform(X)

print("Shape de X_new: ")
print(X.shape)

folds = StratifiedKFold(n_splits=10, random_state=rng)

for train_index, test_index in folds.split(X, y):
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    # TRAINING RANDOM FOREST WITH IMBALANCED DATA
    rf = asc.AutoSklearnClassifier(
        time_left_for_this_task=time+10,
        per_run_time_limit=time,
        ensemble_size=1,
        initial_configurations_via_metalearning=0,
        include_estimators=["random_forest", ], exclude_estimators=None,
        include_preprocessors=["no_preprocessing", ], exclude_preprocessors=None)

    rf.fit(X_train, y_train)
    scores.predict_and_save(rf, X_test, y_test, verbose=True, file="deepFeatsCrossValid/RF.txt")

    # TRAINING SVM WITH IMBALANCED DATA
    svm = autosvm.run(time=time, X=X_train, y=y_train, verbose=True)
    scores.predict_and_save(svm, X_test, y_test, verbose=True, file="deepFeatsCrossValid/SVM.txt")
