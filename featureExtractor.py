from keras.preprocessing import image
from PIL import Image
import os
import gc
import numpy as np
import glob
from ip import resize_centered

config = {'dataset_path': None, 'folders': [], 'model_name': None, 'model_pooling': None}

def createFolder(path):
  if not os.path.isdir(path):
      os.makedirs(path)

def getConfigFromFile(dictionary):
	F = open("config.txt")
	print("Opening.. " + F.name)
	indexVar = 0
	for line in F:
		# Skipping comments and empty lines
		if(line[0] != '#' and line[0] != '\n'):
			# Getting Dataset Path
			if(indexVar == 0):
				# Saving dataset path in the dictionary
				dictionary['dataset_path'] = str(line.replace("\n", ""))
				# Listing directories
				for name in os.listdir(config['dataset_path']):
					if(os.path.isdir(config['dataset_path'] + name)):
						dictionary['folders'].append(name)

			# Getting model name
			if(indexVar == 1):
				dictionary['model_name'] = str(line.replace("\n", ""))

			# Getting model pooling method
			if(indexVar == 2):
				value = str(line.replace("\n", ""))
				if(value != "None"):
					dictionary['model_pooling'] = value

			indexVar += 1

def setConfig(dataset_path=None, model_name=None, model_pooling=None):
	config = {'dataset_path': dataset_path, 'folders': [], 'model_name': model_name, 'model_pooling': model_pooling}
	# Listing directories
	for name in os.listdir(config['dataset_path']):
		if(os.path.isdir(config['dataset_path'] + name)):
			config['folders'].append(name)

	return config


def selectModel(model_name, model_pooling):
	print(model_name + " with " + str(model_pooling) + " pooling..")

	if(model_name == "Xception"):
		from keras.applications.xception import Xception
		from keras.applications.xception import preprocess_input
		model = Xception(weights='imagenet', include_top=False, pooling=model_pooling)

	if(model_name == "VGG16"):
		from keras.applications.vgg16 import VGG16
		from keras.applications.vgg16 import preprocess_input
		model = VGG16(weights='imagenet', include_top=False, pooling=model_pooling)

	if(model_name == "VGG19"):
		from keras.applications.vgg19 import VGG19
		from keras.applications.vgg19 import preprocess_input
		model = VGG19(weights='imagenet', include_top=False, pooling=model_pooling)

	if(model_name == "ResNet50"):
		from keras.applications.resnet50 import ResNet50
		from keras.applications.resnet50 import preprocess_input
		model = ResNet50(weights='imagenet', include_top=False, pooling=model_pooling)

	if(model_name == "InceptionV3"):
		from keras.applications.inception_v3 import InceptionV3
		from keras.applications.inception_v3 import preprocess_input
		model = InceptionV3(weights='imagenet', include_top=False, pooling=model_pooling)

	if(model_name == "InceptionResNetV2"):
		from keras.applications.inception_resnet_v2 import InceptionResNetV2
		from keras.applications.inception_resnet_v2 import preprocess_input
		model = InceptionResNetV2(weights='imagenet', include_top=False, pooling=model_pooling)

	if (model_name == "DenseNet121"):
		from keras.applications.densenet import DenseNet121
		from keras.applications.densenet import preprocess_input
		model = DenseNet121(weights='imagenet', include_top=False, pooling=model_pooling)

	if (model_name == "DenseNet169"):
		from keras.applications.densenet import DenseNet169
		from keras.applications.densenet import preprocess_input
		model = DenseNet169(weights='imagenet', include_top=False, pooling=model_pooling)

	if (model_name == "DenseNet201"):
		from keras.applications.densenet import DenseNet201
		from keras.applications.densenet import preprocess_input
		model = DenseNet201(weights='imagenet', include_top=False, pooling=model_pooling)

	if (model_name == "NASNetLarge"):
		from keras.applications.nasnet import NASNetLarge
		from keras.applications.nasnet import preprocess_input
		model = NASNetLarge(input_shape= (331, 331, 3),weights='imagenet', include_top=False, pooling=model_pooling)

	if (model_name == "NASNetMobile"):
		from keras.applications.nasnet import NASNetMobile
		from keras.applications.nasnet import preprocess_input
		model = NASNetMobile(input_shape= (224, 224, 3), weights='imagenet', include_top=False, pooling=model_pooling)

	model.preprocess_input = preprocess_input

	return model

def extract(img_list=None, model_name="VGG16", model_pooling="max", channels=3, image_size=(224, 224)):
    gray = channels == 1

    model = selectModel(model_name, model_pooling)
    n_images = len(img_list)

    new_size = -1
    count = 1
    features = []
    for img_path in img_list:
        img = image.load_img(img_path, grayscale=gray, target_size=None)
        img_pad = resize_centered(img)
        img = image.img_to_array(img_pad.resize(image_size))

        # print(img_path)
        # img.show()
        # img_pad.show()
        # parar

        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        x = model.preprocess_input(x)
        f = model.predict(x)

        n_dims = f.ndim
        new_size = 1
        for n in range(n_dims):
            new_size *= f.shape[n]

        # print("features shape: ")
        # print(f.shape)
        f = f.reshape(1, new_size)

        print("[" + str(count) + "/" + str(n_images) + "] " + str(f.shape) + " fts from " + img_path + " by " + model_name)
        features.append(f)
        count += 1

    return np.asarray(features).reshape(count-1, new_size)


def toLibSVM(X, y, output_dir, filename):
    root = ""
    if(output_dir!=None):
        createFolder(output_dir)
        root = output_dir + "/"

    Out = open(root + filename, "w")
    j = 0
    for feats in X:
        Out.write(str(y[j]) + " ")
        for i in range(len(feats)):
            Out.write(str(i+1) + ":" + str(feats[i]) + " ")

        Out.write("\n")
        j += 1

    Out.close()


def run(config=None, output_dir=None):
	if(config==None):
		# Getting config file...
		getConfigFromFile(config)

	print(config)

	label = 0
	for folder in config['folders']:
		dataset = glob.glob(config['dataset_path'] + folder + "/*.*")
		print("From folder: " + folder)

		# Extracting features
		print("Extracting..")
		features = extract(dataset, config['model_name'], config['model_pooling'])

		# Resume
		n_features = features[0].shape[1]
		n_files = len(features)
		print("It was extracted " + str(n_features) + " features from " + str(n_files) + " files.")

		# Exporting as LibSVM
		print("Exporting to file...")
		toLibSVM(features, folder, config['model_name'], config['model_pooling'], label, output_dir)
		print("Done!")
		label += 1
		gc.collect()
