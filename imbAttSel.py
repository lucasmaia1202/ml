from collections import Counter
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import SelectFromModel
from sklearn.svm import LinearSVC
import autosklearn.classification as asc
import numpy as np
import random
import setup
import featureExtractor as fe
import smoteenn
import autosvm
import scores

def doExtraction(set, models, pooling):
    X = []
    for m in models:
        feats = np.asarray(fe.extract(set, m, pooling))
        if(len(X) > 0):
            X = np.hstack((np.asarray(X), feats))
        else:
            X = feats

    return X

def main():
    database = '/media/deeplabmint/DADOS/Lucas/Melanoma/PH2/Original/Rois_NM/'
    img_type = 'bmp'

    models = ["ResNet50"]#, "VGG16", "VGG19", "InceptionV3"]
    useAttSel = False
    pooling = "max"
    time = 60*10
    rng = np.random.RandomState()

    runs = 5

    for i in range(runs):

        (train_set, y_train), (test_set, y_test), (valid_set, y_valid) = setup.config_base(database=database, test_prop=0.3, img_type=img_type)

        X_train = doExtraction(train_set, models, pooling)
        X_test = doExtraction(test_set, models, pooling)
        y_train = np.asarray(y_train).reshape(-1, 1)
        y_test = np.asarray(y_test).reshape(-1, 1)


        print("Shape de X_train: ")
        print(X_train.shape)
        print("Shape de X_test: ")
        print(X_test.shape)

        # X = np.vstack((X_train, X_test))
        # y = np.vstack((y_train, y_test))

        svm = autosvm.run(time=time, X=X_train, y=y_train, verbose=True)
        X_st, y_st = smoteenn.run(X=X_train, y=y_train, smote_kind="svm", smote_svm_estimator=svm, verbose=True, save_dist=True, file="resNetImbAttSel/dist.txt")

        # MAKING ATTRIBUTES SELECTION
        if(useAttSel):
            lsvc = LinearSVC(C=0.01, penalty="l1", dual=False).fit(X_st, y_st)
            model = SelectFromModel(lsvc, prefit=True)
            X_train = model.transform(X_train)
            X_test = model.transform(X_test)
            X_st = model.transform(X_st)

            print("Shape de X_train_new: ")
            print(X_train.shape)
            print("Shape de X_test_new: ")
            print(X_test.shape)

        # TRAINING RANDOM FOREST WITH IMBALANCED DATA
        rf = asc.AutoSklearnClassifier(
            time_left_for_this_task=time+10,
            per_run_time_limit=time,
            ensemble_size=1,
            initial_configurations_via_metalearning=0,
            include_estimators=["random_forest", ], exclude_estimators=None,
            include_preprocessors=["no_preprocessing", ], exclude_preprocessors=None)

        rf.fit(X_train, y_train)
        scores.predict_and_save(rf, X_test, y_test, verbose=True, file="resNetImbAttSel/RF.txt")

        # TRAINING RANDOM FOREST WITH BALANCED DATA
        rfb = asc.AutoSklearnClassifier(
            time_left_for_this_task=time+10,
            per_run_time_limit=time,
            ensemble_size=1,
            initial_configurations_via_metalearning=0,
            include_estimators=["random_forest", ], exclude_estimators=None,
            include_preprocessors=["no_preprocessing", ], exclude_preprocessors=None)

        rfb.fit(X_st, y_st)
        scores.predict_and_save(rfb, X_test, y_test, verbose=True, file="resNetImbAttSel/RF_Balanced.txt")

if __name__ == "__main__":
    main()
