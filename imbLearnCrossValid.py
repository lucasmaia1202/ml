from collections import Counter
from sklearn.model_selection import StratifiedKFold
import autosklearn.classification as asc
import numpy as np
import random
import setup
import featureExtractor as fe
import smoteenn
import autosvm
import scores

database = '/media/deeplabmint/DADOS/Lucas/Melanoma/PH2/Original/Rois_NM/'
img_type = 'bmp'

model = "ResNet50"
pooling = "max"
time = 60*15
rng = np.random.RandomState()

(train_set, y_train), (test_set, y_test), (valid_set, y_valid) = setup.config_base(database=database, test_prop=0.0, img_type=img_type)

X = fe.extract(train_set, model, pooling)
y = np.asarray(y_train)

folds = StratifiedKFold(n_splits=10, random_state=rng)

for train_index, test_index in folds.split(X, y):
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    # TRAINING RANDOM FOREST WITH IMBALANCED DATA
    rf = asc.AutoSklearnClassifier(
        time_left_for_this_task=time+10,
        per_run_time_limit=time,
        ensemble_size=1,
        initial_configurations_via_metalearning=0,
        include_estimators=["random_forest", ], exclude_estimators=None,
        include_preprocessors=["no_preprocessing", ], exclude_preprocessors=None)

    rf.fit(X_train, y_train)
    scores.predict_and_save(rf, X_test, y_test, verbose=True, file="imbLearnCrossValid/RF.txt")

    # TRAINING SVM WITH IMBALANCED DATA
    svm = autosvm.run(time=time, X=X_train, y=y_train, verbose=True)
    scores.predict_and_save(svm, X_test, y_test, verbose=True, file="imbLearnCrossValid/SVM.txt")

    # APPLYING SMOTEENN
    X_st, y_st = smoteenn.run(X=X_train, y=y_train, smote_kind="svm", smote_svm_estimator=svm, verbose=True, save_dist=True, file="imbLearnCrossValid/dist.txt")

    # TRAINING RANDOM FOREST WITH BALANCED DATA
    rfb = asc.AutoSklearnClassifier(
        time_left_for_this_task=time+10,
        per_run_time_limit=time,
        ensemble_size=1,
        initial_configurations_via_metalearning=0,
        include_estimators=["random_forest", ], exclude_estimators=None,
        include_preprocessors=["no_preprocessing", ], exclude_preprocessors=None)

    rfb.fit(X_st, y_st)
    scores.predict_and_save(rfb, X_test, y_test, verbose=True, file="imbLearnCrossValid/RF_Balanced.txt")

    # TRAINING SVM WITH BALANCED DATA
    svm_final = autosvm.run(time=time, X=X_st, y=y_st, verbose=True)
    scores.predict_and_save(svm_final, X_test, y_test, verbose=True, file="imbLearnCrossValid/SVM_Balanced.txt")
