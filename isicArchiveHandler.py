import glob
import json
from PIL import Image

#CARREGANDO OS CAMINHOS DAS DESCRICOES DAS IMAGENS
dataset = glob.glob("/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC/ISIC-Archive-Downloader-master/Data/Descriptions/*")
datasize = len(dataset)
dataset = sorted(dataset)
print("Descriptions data size: " + str(datasize))

#CONTADORES PARA M=MALIGNO, B=BENIGNO E MISSING=NAO TEM DIAGNOSTICO
m = 0
b = 0
missing = 0

#PARA CADA ARQUIVO DE DESCRICAO NO DATASET
for d in dataset:
    #ABRE ARQUIVO, PEGA O CONTEUDO E TRANSFORMA EM JSON
    f = open(d, "r")
    content = f.read()
    objson = json.loads(content)

    try:

        #TENTA ACHAR O VALOR QUE DAR O DIAGNOSTICO, CASO NAO ACHE, NAO USAR IMAGEM
        diagnosis = objson["meta"]["clinical"]["benign_malignant"]
        name = objson["name"]

        try:
            #TENTA ABRIR IMAGEM
            img = Image.open("/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC/ISIC-Archive-Downloader-master/Data/Images/"+name+".jpeg")

            try:
                #TENTAR ABRIR SEGMENTACAO EXPERT, CASO NAO ACHE, ABRE NOVICE
                seg = Image.open("/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC/ISIC-Archive-Downloader-master/Data/Segmentations/"+name+"_expert.png")
            except:
                try:
                    seg = Image.open("/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC/ISIC-Archive-Downloader-master/Data/Segmentations/"+name+"_novice.png")
                except:
                    seg = None

            #CONTA E SALVA IMAGEM NO DEVIDO LUGAR
            if(diagnosis == "benign"):
                b += 1
                img.save("/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC/ISIC_Archive/Images/Benign/"+name+".jpeg")
                if(seg != None):
                    seg.save("/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC/ISIC_Archive/Segmentations/Benign/"+name+".png")

            if(diagnosis == "malignant"):
                m += 1
                img.save("/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC/ISIC_Archive/Images/Malignant/"+name+".jpeg")
                if(seg != None):
                    seg.save("/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC/ISIC_Archive/Segmentations/Malignant/"+name+".png")

        except:
            print("Error dealing with image: " + d)

    except:
        missing += 1
        print("Error dealing with: " + d)



    print("(B: " + str(b) + ", M: " + str(m) + ") current: " + d)


#OUTPUTS CONTAGEM
print("Benign:" + str(b))
print("Malignant:" + str(m))
print("Missing:" + str(missing))
