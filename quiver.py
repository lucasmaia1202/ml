from keras.applications.vgg16 import VGG16
from keras.applications.vgg19 import VGG19
from quiver_engine.server import launch

model = VGG16(weights='imagenet', include_top=True, pooling="max")

launch(model, input_folder='dermatoscopias')
