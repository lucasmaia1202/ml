from collections import Counter
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import SelectFromModel
from sklearn.svm import LinearSVC
import autosklearn.classification as asc
import numpy as np
import random
import setup
import featureExtractor as fe
import smoteenn
import autosvm
import scores
import abcd


def doExtraction(set, models, pooling):
    X = []
    for m in models:
        feats = np.asarray(fe.extract(set, m, pooling))
        if(len(X) > 0):
            X = np.hstack((np.asarray(X), feats))
        else:
            X = feats

    return X


def main():
    database = [
        {"url": '/media/deeplabmint/DADOS/Lucas/Melanoma/PH2/Original/Rois_NM/', "img_type": "bmp"},
        # {"url": '/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC_2016/', "img_type": "jpg"}
    ]

    models = ["ResNet50"]  # , "VGG16", "VGG19", "InceptionV3"]
    useAttSel = True
    pooling = "max"
    time = 60*60
    rng = np.random.RandomState()

    runs = 10

    for i in range(runs):

        print("Dividingo a base em Treino/Test")

        (train_set, y_train), (test_set, y_test), (valid_set, y_valid) = setup.config_base(
            database=database, test_prop=0.3)

        print("Extraindo caracteristica com ResNet50")

        X_resnet_train = doExtraction(train_set, models, pooling)
        X_resnet_test = doExtraction(test_set, models, pooling)

        print("Extraindo caracteristicas ABCD")

        X_abcd_train = abcd.doABCDExtraction(train_set)  # ACRESCENTEI AQUI
        X_abcd_test = abcd.doABCDExtraction(test_set)  # ACRESCENTEI AQUI

        X_train = np.hstack((X_resnet_train, X_abcd_train))
        X_test = np.hstack((X_resnet_test, X_abcd_test))

        y_train = np.asarray(y_train).reshape(-1, 1)
        y_test = np.asarray(y_test).reshape(-1, 1)


        # APPLYING SMOTEENN
        print("Estimando SVM para SMOTEENN: Run #" + str(i+1))
        svm = autosvm.run(time=time, X=X_train, y=y_train, verbose=True)
        X_st, y_st = smoteenn.run(X=X_train, y=y_train, smote_kind="svm", smote_svm_estimator=svm,
                                  verbose=True, save_dist=True, file="resNetABCDImb/dist.txt")

        # MAKING ATTRIBUTES SELECTION
        if(useAttSel):
            print("Usando Seletor de Atributos: Run #" + str(i+1))
            lsvc = LinearSVC(C=100.0, penalty="l1", dual=False).fit(X_st, y_st)
            model = SelectFromModel(lsvc, prefit=True)
            X_train = model.transform(X_train)
            X_test = model.transform(X_test)
            X_st = model.transform(X_st)

            print("Shape de X_train_new: ")
            print(X_train.shape)
            print("Shape de X_test_new: ")
            print(X_test.shape)
            print("Shape de X_st_new: ")
            print(X_st.shape)

            with open("resNetABCDImb/shapes.txt", "a") as arch:
                arch.write("Train: " + str(X_train.shape) + ", Test: " + str(X_test.shape) + ", Smoteenn: " + str(X_st.shape) + "\n")

        # TRAINING RANDOM FOREST WITH IMBALANCED DATA
        print("Estimando Random Forest para dados Desbalanceados: Run #" + str(i+1))
        rf = asc.AutoSklearnClassifier(
            time_left_for_this_task=time+10,
            per_run_time_limit=time,
            ensemble_size=1,
            initial_configurations_via_metalearning=0,
            include_estimators=["random_forest", ], exclude_estimators=None,
            include_preprocessors=["no_preprocessing", ], exclude_preprocessors=None)

        rf.fit(X_train, y_train)
        scores.predict_and_save(rf, X_test, y_test, verbose=True, file="resNetABCDImb/RF.txt")

        # TRAINING RANDOM FOREST WITH BALANCED DATA
        print("Estimando Random Forest para dados Balanceados: Run #" + str(i+1))
        rfb = asc.AutoSklearnClassifier(
            time_left_for_this_task=time+10,
            per_run_time_limit=time,
            ensemble_size=1,
            initial_configurations_via_metalearning=0,
            include_estimators=["random_forest", ], exclude_estimators=None,
            include_preprocessors=["no_preprocessing", ], exclude_preprocessors=None)

        rfb.fit(X_st, y_st)
        scores.predict_and_save(rfb, X_test, y_test, verbose=True, file="resNetABCDImb/RF_Balanced.txt")


    print("\n\nCABOU!!\n\n")


if __name__ == "__main__":
    main()
