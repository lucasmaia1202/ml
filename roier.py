import os
import glob
import cv2
import numpy as np
import pandas

def find_name(path):
    init = path.rfind("/") + 1
    last = path.rfind(".")
    return path[init:last]

def crop_roi(img, mask):
    mask = mask>0
    return img[np.ix_(mask.any(1),mask.any(0))]

def split_classses(images, labels, classes_output):
    labels = pandas.read_csv(labels).values
    images_list = sorted(glob.glob(images["url"] + "/*." + images["img_type"]))

    for i in range(len(images_list)):
        img_name = find_name(images_list[i])
        img_class = labels[i][1]

        output_dir = classes_output + "/" + str(img_class)

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        complete_path = output_dir + "/" + img_name + "." + images["img_type"]

        print(complete_path)

        img = cv2.imread(images_list[i])
        cv2.imwrite(complete_path, img)

def run(images=None, masks=None, output_dir=None, verbose=False):
    if(images == None or masks == None):
        print("Images or Masks has not been informed!")
        return None

    images_list = glob.glob(images["url"] + "/*." + images["img_type"])
    masks_list = glob.glob(masks["url"] + "/*." + masks["img_type"])
    images_list = sorted(images_list)
    masks_list = sorted(masks_list)

    if(len(images_list) != len(masks_list)):
        print("Images and Masks lists does not have same length.")
        return None

    if output_dir == None:
        output_dir = images + "/../" + "Rois_autocreated"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for i in range(len(images_list)):
        img = cv2.imread(images_list[i])
        mask = cv2.imread(masks_list[i], 0)
        res = cv2.bitwise_and(img, img, mask=mask)

        # cv2.imshow("normal", res)

        res = crop_roi(res, mask)
        # cv2.imshow("cropped", res)
        # cv2.waitKey(0)
        # para

        complete_path = output_dir + "/" + find_name(images_list[i]) + "." + images["img_type"]

        if(verbose):
            print("Saving images " + str(i+1) + ": " + complete_path)

        cv2.imwrite(complete_path, res)

    print("\n\nCabou!!\n\n")


def main():
    images = {"url": "/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC_2016/Imagens", "img_type": "jpg"}
    masks = {"url": "/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC_2016/Masks", "img_type": "png"}
    rois_output = "/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC_2016/RoisNew"

    input = {"url": rois_output, "img_type": "jpg"}
    labels = "/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC_2016/labels.csv"
    classes_output = "/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC_2016/RoisSep"

    roier.run(images, masks, rois_output)
    roier.split_classses(input, labels, classes_output)


if __name__ == "__main__":
    main()
