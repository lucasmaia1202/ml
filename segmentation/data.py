from __future__ import print_function
import os
import numpy as np
import setup
import ip

def get_name(path):
    indx = path.rfind('/') + 1
    dot = path.rfind('.')
    name = path[indx:dot]
    return name

def get_normals(dataset, masks_set):
    normals = []
    for m in masks_set:
        name = get_name(m)
        path = dataset["url"] + name + "." + dataset["img_type"]
        normals.append(path)

    return normals

def config():
    masks = [
        # {"url": '/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC/ISIC_Archive/Segmentations/', "img_type": "png"},
        # {"url": '/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC/ISIC_2018/ISIC2018_Task1_Training_GroundTruth/', "img_type": "png"},
        {"url": '/media/deeplabmint/DADOS/Lucas/Melanoma/PH2/Original/Masks/', "img_type": "bmp"},
    ]

    # normals = {"url": '/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC/ISIC_Archive/Images/', "img_type": "jpeg"}
    # normals = {"url": '/media/deeplabmint/DADOS/Lucas/Melanoma/ISIC/ISIC_2018/ISIC2018_Task1-2_Training_Input/', "img_type": "jpg"}
    normals = {"url": '/media/deeplabmint/DADOS/Lucas/Melanoma/PH2/Original/Dermatoscopia/', "img_type": "bmp"}

    img_rows = 256
    img_cols = 256
    channels = 3

    (train_set, y_train), (test_set, y_test), (valid_set, y_valid) = setup.config_base(database=masks, test_prop=0.3)
    normal_train_set = get_normals(normals, train_set)
    normal_test_set = get_normals(normals, test_set)

    masks_train = ip.list_to_array(train_set, (img_rows, img_cols), 1)
    print(masks_train.shape)
    np.save('dump/imgs_mask_train.npy', masks_train)

    masks_test = ip.list_to_array(test_set, (img_rows, img_cols), 1)
    print(masks_test.shape)
    np.save('dump/imgs_mask_test.npy', masks_test)

    normals_train = ip.list_to_array(normal_train_set, (img_rows, img_cols), channels)
    print(normals_train.shape)
    np.save('dump/imgs_train.npy', normals_train)

    normals_test = ip.list_to_array(normal_test_set, (img_rows, img_cols), channels)
    print(normals_test.shape)
    np.save('dump/imgs_test.npy', normals_test)

def load_train_data():
    imgs_train = np.load('dump/imgs_train.npy')
    imgs_mask_train = np.load('dump/imgs_mask_train.npy')
    return imgs_train, imgs_mask_train

def load_test_data():
    imgs_test = np.load('dump/imgs_test.npy')
    imgs_mask_test = np.load('dump/imgs_mask_test.npy')
    return imgs_test, imgs_mask_test

if __name__ == '__main__':
    config()
