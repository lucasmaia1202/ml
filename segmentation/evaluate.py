import numpy as np
import math
import metrics
import sys
from data import load_test_data
from unet import UNet, UNetDrop

def calc_scores(imgs_mask_test, imgs_mask_predict):
    f_eval = open("dump/evaluation.csv", 'w')
    f_eval.write('Img, Accuracy, Dice, Jaccard, Sensitivity, Specificity'+'\n')

    print('-'*30)
    print('Segmentation evaluation...')
    print('-'*30)

    print('Img,Accuracy,Dice,Jaccard,Sensitivity,Specificity')

    # Initialization
    sum_prec = 0
    sum_dice = 0
    sum_jacc = 0
    sum_sensitivity = 0
    sum_specificity = 0

    total = 0
    test_size = len(imgs_mask_test)
    for i in range(test_size):

        try:
            y_true = imgs_mask_test[i].reshape(256*256*1, 1)
            y_pred = imgs_mask_predict[i].reshape(256*256*1, 1)
            y_true = np.where(y_true < 0.5, 0, 1)
            y_pred = np.where(y_pred < 0.5, 0, 1)

            # Compute Dice coefficient
            dice = metrics.dice_score(y_true, y_pred)
            sum_dice = sum_dice + dice

            # Compute Jaccard similarity coefficient score
            jacc = metrics.jaccard_score(y_true, y_pred)
            sum_jacc = sum_jacc + jacc

            # Compute the Sensitivity (recall)
            sensitivity = metrics.sensitivity(y_true, y_pred)
            if(math.isnan(sensitivity)):
                sensitivity = 0.0
            sum_sensitivity = sum_sensitivity + sensitivity

            # Compute Specificity score.
            specificity = metrics.specificity(y_true, y_pred)
            sum_specificity = sum_specificity + specificity

            # Compute Precision
            prec = metrics.precision(y_true, y_pred)
            sum_prec = sum_prec + prec

            f_eval.write("{:d},{:2f},{:2f},{:2f},{:2f},{:2f}\n".format(i,dice,jacc,prec,sensitivity,specificity))
            print("({:d}/{:d}),{:2f},{:2f},{:2f},{:2f},{:2f}\n".format(i,test_size,dice,jacc,prec,sensitivity,specificity))

            total += 1
        except Exception as e:
            print("error: ")
            print(e)

    avg_prec = sum_prec / total
    avg_dice = sum_dice / total
    avg_jacc = sum_jacc / total
    avg_sensitivity = sum_sensitivity / total
    avg_specificity = sum_specificity / total

    f_eval.write("\n\nAverage evaluation: {:2f}\t{:2f}\t{:2f}\t{:2f}\t{:2f}\n".format(avg_dice,avg_jacc,avg_prec,avg_sensitivity,avg_specificity))
    f_eval.close()

    print("\n\nAverage evaluation: {:2f}\t{:2f}\t{:2f}\t{:2f}\t{:2f}\n".format(avg_dice,avg_jacc,avg_prec,avg_sensitivity,avg_specificity))


def predict(imgs_test, img_shape):
    if(len(sys.argv) > 1):
        if(sys.argv[1] == '-l'):
            return np.load('dump/imgs_mask_predict.npy')

    model = UNetDrop(img_shape)
    print('-'*30)
    print('Loading saved weights...')
    print('-'*30)
    model.load_weights('dump/unet_95.hdf5')

    print('-'*30)
    print('Predicting masks on test data...')
    print('-'*30)
    imgs_mask_predict = model.predict(imgs_test, verbose=1)
    np.save('dump/imgs_mask_predict.npy', imgs_mask_predict)

    return imgs_mask_predict

def main():
    print('-'*30)
    print('Loading and preprocessing test data...')
    print('-'*30)
    #imgs_test, imgs_id_test = load_test_data()
    imgs_test, imgs_mask_test = load_test_data()

    print(imgs_test.shape)
    print(imgs_mask_test.shape)

    n = imgs_test.shape[0]
    img_rows = imgs_test.shape[1]
    img_cols = imgs_test.shape[2]
    channels = imgs_test.shape[3]
    img_shape = (img_rows, img_cols, channels)

    calc_scores(imgs_mask_test, predict(imgs_test, img_shape))

if __name__ == '__main__':
    main()
