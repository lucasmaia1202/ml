from __future__ import print_function
import sklearn as sk
import numpy as np
from keras import backend as K
K.set_image_data_format('channels_last')

smooth = 1.

def dice_coef(y_true, y_pred):
    print(y_true.shape)
    print(y_pred.shape)
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)

def dice_coef_loss(y_true, y_pred):
    return -dice_coef(y_true, y_pred)

def jaccard_coef(y_true, y_pred):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (intersection + 1.0) / (K.sum(y_true_f) + K.sum(y_pred_f) - intersection + 1.0)

def jaccard_coef_loss(y_true, y_pred):
    return -jacard_coef(y_true, y_pred)

def dice_score(y_true, y_pred):
    im1 = np.asarray(y_true).astype(np.bool)
    im2 = np.asarray(y_pred).astype(np.bool)
    intersection = np.logical_and(im1, im2)
    dice = (2. * intersection.sum() + smooth) / (im1.sum() + im2.sum() + smooth)
    return dice

def jaccard_score(y_true, y_pred):
    return sk.metrics.jaccard_similarity_score(y_true, y_pred)

def sensitivity(y_true, y_pred):
    return sk.metrics.recall_score(y_true, y_pred)

def specificity(y_true, y_pred):
    cm = sk.metrics.confusion_matrix(y_true, y_pred)
    tn, fp, fn, tp = cm.ravel()
    spec = tn/float(tn + fp)
    return spec

def precision(y_true, y_pred):
    return sk.metrics.precision_score(y_true, y_pred)
