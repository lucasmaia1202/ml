from __future__ import print_function

from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from data import load_train_data
from unet import UNet, UNetDrop
import os

nb_epoch = 150
#Unet - Done: 250-87, 400-89, 550-90
#UnetDrop - Done: 100-87, 250-91, 400-94, 550-95

def main():
    print('-'*30)
    print('Loading and preprocessing train data...')
    print('-'*30)
    imgs_train, imgs_mask_train = load_train_data()

    print(imgs_train.shape)
    print(imgs_mask_train.shape)

    n = imgs_train.shape[0]
    img_rows = imgs_train.shape[1]
    img_cols = imgs_train.shape[2]
    channels = imgs_train.shape[3]
    img_shape = (img_rows, img_cols, channels)

    print('-'*30)
    print('Creating and compiling model...' + str(n) + ' images')
    print('-'*30)
    model = UNetDrop(img_shape)

    if(os.path.exists('dump/unet_94.hdf5')):
        model.load_weights('dump/unet_94.hdf5')

    model_checkpoint = ModelCheckpoint(filepath='dump/unet.hdf5', monitor='loss', save_best_only=True)

    print('-'*30)
    print('Fitting model...')
    print('-'*30)
    model.fit(
        imgs_train,
        imgs_mask_train,
        batch_size=16,
        epochs=nb_epoch,
        verbose=1,
        shuffle=True,
        callbacks=[model_checkpoint])

if __name__ == '__main__':
    main()
