from __future__ import print_function

from keras.models import Model
from keras.layers import Input, concatenate, Conv2D, MaxPooling2D, Conv2DTranspose, Flatten, Dense, BatchNormalization, Dropout
from keras.layers.core import Dropout, Lambda
from keras.optimizers import Adam, SGD
from metrics import dice_coef, dice_coef_loss

def UNet(img_shape=(256, 256, 3)):
    #unet usada por nelia na base de olho
    inputs = Input(img_shape)
    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same',data_format="channels_last")(inputs)

    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same',data_format="channels_last")(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2),data_format="channels_last")(conv1)

    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format="channels_last")(pool1)
    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format="channels_last")(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2),data_format="channels_last")(conv2)

    conv3 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format="channels_last")(pool2)
    conv3 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format="channels_last")(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2),data_format="channels_last")(conv3)

    conv4 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format="channels_last")(pool3)
    conv4 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format="channels_last")(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2),data_format="channels_last")(conv4)

    conv5 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format="channels_last")(pool4)
    conv5 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format="channels_last")(conv5)

    up6 = concatenate([Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(conv5), conv4], axis=3)
    conv6 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format="channels_last")(up6)
    conv6 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format="channels_last")(conv6)

    up7 = concatenate([Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(conv6), conv3], axis=3)
    conv7 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format="channels_last")(up7)
    conv7 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format="channels_last")(conv7)

    up8 = concatenate([Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(conv7), conv2], axis=3)
    conv8 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format="channels_last")(up8)
    conv8 = Conv2D(64, (3, 3), activation='relu', padding='same',data_format="channels_last")(conv8)

    up9 = concatenate([Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same')(conv8), conv1], axis=3)
    conv9 = Conv2D(32, (3, 3), activation='relu', padding='same',data_format="channels_last")(up9)
    conv9 = Conv2D(32, (3, 3), activation='relu', padding='same',data_format="channels_last")(conv9)

    conv10 = Conv2D(1, (1, 1), activation='sigmoid',data_format="channels_last")(conv9)
    # flatten =  Flatten()(conv9)
    # Dense1 = Dense(512, activation='relu')(flatten)
    # BN =BatchNormalization() (Dense1)
    # Dense2 = Dense(17, activation='sigmoid')(BN)

    model = Model(inputs=inputs, outputs=conv10)

    #sgd = SGD(lr=0.001, decay=3e-4, momentum=0.95, nesterov=True)
    #model.compile(optimizer=sgd, loss=dice_coef_loss, metrics=[dice_coef])
    model.compile(optimizer=Adam(lr=1e-5), loss=dice_coef_loss, metrics=[dice_coef])
    # model.compile(optimizer=Adam(lr=1e-5), loss=dice_coef_loss, metrics=[dice_coef, jacard, precision, recall])
    return model

def UNetDrop(img_shape=(256, 256, 3)):
    #unet usada por bruno na base ph2 e isic de melanoma
    inputs = Input(img_shape)
    s = Lambda(lambda x: x / 255) (inputs)


    c1 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (s)
    c1 = Dropout(0.1) (c1)
    c1 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (c1)
    p1 = MaxPooling2D((2, 2)) (c1)

    c2 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (p1)
    c2 = Dropout(0.1) (c2)
    c2 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (c2)
    p2 = MaxPooling2D((2, 2)) (c2)

    c3 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (p2)
    c3 = Dropout(0.2) (c3)
    c3 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (c3)
    p3 = MaxPooling2D((2, 2)) (c3)

    c4 = Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (p3)
    c4 = Dropout(0.2) (c4)
    c4 = Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (c4)
    p4 = MaxPooling2D(pool_size=(2, 2)) (c4)

    c5 = Conv2D(512, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (p4)
    c5 = Dropout(0.3) (c5)
    c5 = Conv2D(512, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (c5)

    u6 = Conv2DTranspose(256, (2, 2), strides=(2, 2), padding='same') (c5)
    u6 = concatenate([u6, c4])
    c6 = Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (u6)
    c6 = Dropout(0.2) (c6)
    c6 = Conv2D(256, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (c6)

    u7 = Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same') (c6)
    u7 = concatenate([u7, c3])
    c7 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (u7)
    c7 = Dropout(0.2) (c7)
    c7 = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (c7)

    u8 = Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same') (c7)
    u8 = concatenate([u8, c2])
    c8 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (u8)
    c8 = Dropout(0.1) (c8)
    c8 = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (c8)

    u9 = Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same') (c8)
    u9 = concatenate([u9, c1], axis=3)
    c9 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (u9)
    c9 = Dropout(0.1) (c9)
    c9 = Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_normal', padding='same') (c9)

    outputs = Conv2D(1, (1, 1), activation='sigmoid') (c9)

    model = Model(inputs=[inputs], outputs=[outputs])
    # model.compile(optimizer='adam',loss='binary_crossentropy', metrics=[jacard_coef])
    model.compile(optimizer=Adam(lr=1e-5), loss=dice_coef_loss, metrics=[dice_coef])
    return model

def main():
    model = UNet()
    return model

if __name__ == '__main__':
    main()
