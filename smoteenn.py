from collections import Counter
from imblearn.over_sampling import SMOTE
from imblearn.under_sampling import EditedNearestNeighbours
from sklearn.svm import SVC

def run(X=None, y=None, random_state=42, smote_ratio="minority", smote_kind="regular", smote_svm_estimator=None, enn_ratio="all", enn_kind_sel="all", enn_n_neighbors=3, verbose=False, save_dist=False, file=None):

    sm = None

    if(smote_kind == "svm" and smote_svm_estimator != None):
        sm = SMOTE(random_state=random_state, ratio=smote_ratio, kind=smote_kind, svm_estimator=smote_svm_estimator)
    elif(smote_kind == "svm" and smote_svm_estimator == None):
        sm = SMOTE(random_state=random_state, ratio=smote_ratio, kind=smote_kind, svm_estimator=SVC())
    else:
        sm = SMOTE(random_state=random_state, ratio=smote_ratio, kind=smote_kind)

    enn = EditedNearestNeighbours(random_state=random_state, ratio=enn_ratio, kind_sel=enn_kind_sel, n_neighbors=enn_n_neighbors)

    X_sm, y_sm = sm.fit_sample(X, y)

    if(save_dist):
        with open(file, "a") as arch:
           arch.write("SMOTE: " + str(Counter(y_sm)) + " ")

    X_st, y_st = enn.fit_sample(X_sm, y_sm)

    if(save_dist):
        with open(file, "a") as arch:
            arch.write("ENN:" + str(Counter(y_st))+"\n")

    if(verbose):
        print('Class distribution of the NEW TRAINING set: {}'.format(Counter(y_st)))

    return X_st, y_st
