from collections import Counter
import setup
import featureExtractor as fe
import smoteenn
import autosvm
import wekaHelper as wh

time = 60*60
useAttSel = False
useSmoteenn = False
wekaDump = "deepFeatsLibSVM/weka/attSel/pre/greedy_ph2isic.txt"

(X_train, y_train), (X_test, y_test), (X_valid, y_valid) = setup.config_base(database="deepFeatsLibSVM/dfAbcdIsic.libsvm", test_prop=0.25)

if(useAttSel):
    mapa_selection = wh.get_selected_attributes_from_file(wekaDump, 1)
    X_train = X_train[:, mapa_selection==1]
    X_test = X_test[:, mapa_selection==1]

    print("Shape de X_train_new: ")
    print(X_train.shape)

    with open("deepFeatsLibSVM/gaMilagroso/shapes.txt", "a") as arch:
        arch.write("Shape: " + str(X_train.shape) + "\n")

fe.toLibSVM(X_train, y_train, "deepFeatsLibSVM/gaMilagroso/isic", "dfAbcd_train.libsvm")
fe.toLibSVM(X_test, y_test, "deepFeatsLibSVM/gaMilagroso/isic", "dfAbcd_test.libsvm")

if(useSmoteenn):
    svm = autosvm.run(time=time, X=X_train, y=y_train, verbose=True)
    X_st, y_st = smoteenn.run(X=X_train, y=y_train, verbose=True, save_dist=False)
    X_st, y_st = setup.randomize(X_st, y_st)

    fe.toLibSVM(X_st, y_st, "deepFeatsLibSVM/gaMilagroso", "dfAbcdPh2IsicGreedy_smoteen.libsvm")
