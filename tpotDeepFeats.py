from tpot import TPOTClassifier
from sklearn.metrics.scorer import make_scorer
from sklearn.metrics import cohen_kappa_score
import numpy as np
import random
import setup
import scores
import wekaHelper as wh

def kappa_score(y_true, y_pred):
    return cohen_kappa_score(y_true, y_pred)

def main():
    database = "deepFeatsLibSVM/dfAbcdPh2.libsvm"

    (X_train, y_train), (X_test, y_test), (X_valid, y_valid) = setup.config_base(database=database, test_prop=0.5)

    mapa_selection = wh.get_selected_attributes_from_file("deepFeatsLibSVM/weka/attSel/pre/greedy_ph2.txt", 1)
    X_train = X_train[:, mapa_selection==1]
    X_test = X_test[:, mapa_selection==1]

    print("Shape de X_train_new: ")
    print(X_train.shape)
    print("Shape de X_test_new: ")
    print(X_test.shape)

    with open("deepFeatsAttSelImb/ph2/greedy_1/split_50/shapes.txt", "a") as arch:
        arch.write("Train: " + str(X_train.shape) + ", Test: " + str(X_test.shape) + "\n")

    my_custom_scorer = make_scorer(kappa_score, greater_is_better=True)

    my_tpot = TPOTClassifier(generations=20, population_size=50, cv=10, n_jobs=2, scoring=my_custom_scorer, verbosity=2)
    my_tpot.fit(X_train, y_train)

    print(my_tpot.score(X_test, y_test))

    my_tpot.export('deepFeatsAttSelImb/ph2/greedy_1/split_50/tpotDeepFeatsResKappa.py')

    print("\n\nCABOU!!\n\n")

    scores.predict_and_save(my_tpot, X_test, y_test, verbose=True, file="deepFeatsAttSelImb/ph2/greedy_1/split_50/TPOTScores.txt")


if __name__ == "__main__":
    main()
