from collections import Counter
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import SelectFromModel
from sklearn.svm import LinearSVC
import autosklearn.classification as asc
import numpy as np
import random
import setup
import featureExtractor as fe
import smoteenn
import autosvm
import scores
import abcd
import wekaHelper as wh


def main():

    time = 60*60*1
    rng = np.random.RandomState()

    (X_train, y_train), (t1_set, y_t1), (t2_set, y_t2) = setup.config_base(database="deepFeatsLibSVM/gaMilagroso/isic/results/NormBalan_train.libsvm", test_prop=0.0)
    (X_test, y_test), (t1_set, y_t1), (t2_set, y_t2) = setup.config_base(database="deepFeatsLibSVM/gaMilagroso/isic/dfAbcdNorm_test.libsvm", test_prop=0.0)

    print("Estimando Random Forest:")
    rf = asc.AutoSklearnClassifier(
        time_left_for_this_task=time+10,
        per_run_time_limit=time,
        ensemble_size=1,
        initial_configurations_via_metalearning=0,
        include_estimators=["random_forest", ], exclude_estimators=None,
        include_preprocessors=["no_preprocessing", ], exclude_preprocessors=None)

    rf.fit(X_train, y_train)
    scores.predict_and_save(rf, X_test, y_test, verbose=True, file="deepFeatsLibSVM/gaMilagroso/isic/results/RF_Balanced.txt")

    print("Estimando SVM")
    svm = autosvm.run(time=time+10, X=X_train, y=y_train, verbose=True)
    scores.predict_and_save(svm, X_test, y_test, verbose=True, file="deepFeatsLibSVM/gaMilagroso/isic/results/SVM_Balanced.txt")

    print("\n\nCABOU!!\n\n")


if __name__ == "__main__":
    main()
