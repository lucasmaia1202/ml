import numpy as np


def get_selected_attributes_from_file(file_path=None, threshold=0):
    if(file_path==None):
        print("you must define an file_path parameter: file_path is the file where weka buffer was saved")
        return None

    binary_map = []
    features_id = []

    file = open(file_path, "r")
    for line in file.readlines():
        line = line.strip()
        data = line.split(' ')

        features_id.append(int(data[1]))

        if(float(data[0]) >= threshold):
            binary_map.append(1)
        else:
            binary_map.append(0)

    zipped = list(zip(features_id, binary_map))
    zipped.sort(key = lambda t: t[0])
    features_id, binary_map = zip(*zipped)

    return np.asarray(binary_map)



# mapa = get_selected_attributes_from_file("deepFeatsLibSVM/preproc_rankSearch_weka.txt", 6)
# print(mapa)
